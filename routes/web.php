<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('carga','EjemploController');

Route::get('ejemplo', function()
{
    return view('carga.ejemploC');
});

Route::get('agregar-encuesta', function()
{
    return view('carga.encuesta.agregar-encuesta');
});

Route::get('dashboard', function()
{
    return view('carga.dashboard.dashboard');
});