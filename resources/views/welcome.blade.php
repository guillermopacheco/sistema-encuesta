<!DOCTYPE html>
<html lang="en">
<head>
<title>Inicio</title>
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/bootstrap/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/fonts/circular-std/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/libs/css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/charts/chartist-bundle/chartist.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/charts/morris-bundle/morris.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/charts/c3charts/c3.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset ('plugins/assets/vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <script src="{{asset('plugins/js/jquery.min.js') }}"></script>
</head>
<body class="container-full">
<div class="row">
    <div class="col-md-12">
        @include('structure/header')
    </div>

     <div class="col-md-3">
        @include('structure/sidebar')
    </div>
 <div class="col-md-8">
          @include('contenido/body')  
            @yield('content')
    </div>


</div>

<script src="{{asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script> 
<script src="{{asset('assets/vendor/jquery/jquery-3.3.1.min.js') }}"></script> 
    <!-- bootstap bundle js -->
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script> 
    <!-- slimscroll js -->
    <script src="{{asset('assets/vendor/slimscroll/jquery.slimscroll.js') }}"></script> 
    <!-- main js -->
    <script src="{{asset('assets/libs/js/main-js.js') }}"></script> 
    <!-- chart chartist js -->
    <script src="{{asset('assets/vendor/charts/chartist-bundle/chartist.min.js') }}"></script> 
    <!-- sparkline js -->
    <script src="{{asset('assets/vendor/charts/sparkline/jquery.sparkline.js') }}"></script> 
    <!-- morris js -->
    <script src="{{asset('assets/vendor/charts/morris-bundle/raphael.min.js') }}"></script> 
    <script src="{{asset('assets/vendor/charts/morris-bundle/morris.js') }}"></script> 
    <!-- chart c3 js -->
    <script src="{{asset('assets/vendor/charts/c3charts/c3.min.js') }}"></script> 
    <script src="{{asset('assets/vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script> 
    <script src="{{asset('assets/vendor/charts/c3charts/C3chartjs.js') }}"></script> 
    <script src="{{asset('assets/libs/js/dashboard-ecommerce.js') }}"></script> 

</body>
</html>